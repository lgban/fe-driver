import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import {Socket} from 'phoenix';

let socket = new Socket("ws://localhost:4000/socket", {params: {userToken: "123"}});
socket.connect();

export default class App extends React.Component {
  state = {
    open: false,
    pickup_address: 'Cholula',
    dropoff_address: 'CAPU'
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleReject = () => {

  };
  
  handleAccept = () => {

  };

  componentDidMount() {
    let channel = socket.channel("driver:merry", {});
    channel.join()
      .receive("ok", ({messages}) => console.log("catching up", messages) )
      .receive("error", ({reason}) => console.log("failed join", reason) )
      .receive("timeout", () => console.log("Networking issue. Still waiting..."));

    channel.on("request", msg => {
      console.log(msg);
      this.setState({open: true, pickup_address: msg.pickup_address, dropoff_address: msg.dropoff_address});
    });
  }

  render() {
    return (
      <div>
        <Button variant="outlined" color="primary" onClick={this.handleClickOpen}>
          Open form dialog
        </Button>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Subscribe</DialogTitle>
          <DialogContent>
            <TextField
              autoFocus
              margin="dense"
              id="pickup_address"
              value={this.state.pickup_address}
              label="Pickup Address"
              type="text"
              InputProps={{
                readOnly: true
              }}
              variant="filled"
            />
            <TextField
              autoFocus
              margin="dense"
              id="dropoff_address"
              value={this.state.dropoff_address}
              label="Drop-off Address"
              type="text"
              InputProps={{
                readOnly: true
              }}
              variant="filled"
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleAccept} color="primary">
              Accept
            </Button>
            <Button onClick={this.handleReject} color="secondary">
              Reject
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}